<?php
/**
 * Created by PhpStorm.
 * User: sowersm
 * Date: 3/7/15
 * Time: 8:38 AM
 */

namespace RCSI\Filesystem\Uploads;

use RCSI\Exceptions\UploadException;

class Upload {

    private static $instance;
    private static $uploadPath = "/tmp";
    private static $uploadPermissions = 0755;

    public static function init()
    {
        if(self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public static function setPath($path)
    {
        self::$uploadPath = $path;
    }

    private function __construct()
    {

    }

    public function upload()
    {
        if (empty($_FILES)) {
            throw new UploadException("No files have been uploaded.");
        }

        foreach ($_FILES as $file) {
            if (is_uploaded_file($file["tmp_name"])) {
                $newPath = self::$uploadPath . "/{$file['name']}";
                if (move_uploaded_file($file["tmp_name"], $newPath)) {
                    chmod($newPath, self::$uploadPermissions);
                } else {
                    throw new UploadException(
                        "File : {$file['name']} could not be uploaded."
                    );
                }
            }
        }
    }
}
